package com.example.demo.Controller;

import com.example.demo.repository.CustomerRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.CrossOrigin;
import com.example.demo.domain.Balance;
import com.example.demo.domain.Customer;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@RestController

@CrossOrigin(origins = "*", allowedHeaders = "*", maxAge = 3600)


public class HelloController {

    @RequestMapping (value = "/hello",method = RequestMethod.GET)
    public String hello() {
        return "hello world";
    }
@Autowired
private final CustomerRepository customerRepository;

    public HelloController(CustomerRepository customerRepository) {
        this.customerRepository = customerRepository;


    }

    @RequestMapping (value = "/balance",method = RequestMethod.POST)
    public Balance getBalance(@RequestBody Customer customer){
        Balance balance = new Balance();
        balance.setCustomer(customer);
        /*System.out.println(balance);*/
        customerRepository.save(customer);
        return balance;

    }



}
